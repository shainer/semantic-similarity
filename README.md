# README #

This was part of the Palmisano NLP system for tweets; it trains a machine learning model to derive the semantic similarity of pairs of tweets as a floating point score between 0 and 5. Some example tweets are provided in the repository.